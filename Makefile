DIRS = $(shell find -not -path '*/\.*' -not -path '*results*' -type d \( ! -iname ".*" \))
RISCV_DIRS = ML2_BW_ldst MM_st ML2 MIM2 MCS 
export LANG=en_US.UTF-8

all:
	@for dir in $(DIRS); do \
		if [ -d $$dir ]; then \
			(cd $$dir && $(MAKE)) || exit 1 ; \
		fi \
	done

x280:
	@for dir in $(RISCV_DIRS); do \
		if [ -d $$dir ]; then \
			(cd $$dir && $(MAKE)) || exit 1 ; \
		fi \
	done

clean:
	@for dir in $(DIRS); do \
		if [ -d $$dir ]; then \
			(cd $$dir && $(MAKE) clean) || exit 1 ; \
		fi \
	done

run:
	./run_benchmarks

perf:
	./run_perfstats

.phony: all x280 clean run perf

