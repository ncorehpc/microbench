================ microbench ==============
Date: Tue Dec 7 01:36:18 UTC 2021
Kernel: Linux lithium 5.10.0-g904761e4b851 #1 SMP Tue May 11 12:46:23 PDT 2021 aarch64 GNU/Linux
   Static hostname: lithium
         Icon name: computer
        Machine ID: f5be09ebdf96447fbaac764ccce52102
           Boot ID: a154bcb9d239448c82276f5252ded362
  Operating System: Lithium 10
            Kernel: Linux 5.10.0-g904761e4b851
      Architecture: arm64

Architecture:        aarch64
Byte Order:          Little Endian
CPU(s):              4
On-line CPU(s) list: 0-3
Thread(s) per core:  1
Core(s) per socket:  4
Socket(s):           1
Vendor ID:           ARM
Model:               4
Model name:          Cortex-A53
Stepping:            r0p4
CPU max MHz:         1199.9990
CPU min MHz:         299.9990
BogoMIPS:            199.98
Flags:               fp asimd evtstrm aes pmull sha1 sha2 crc32 cpuid

-- Compiler options:

# Compiler
CC=aarch64-linux-gnu-gcc

# Optimized Flags
OPT=-Ofast -march=armv8-a+simd -mtune=cortex-a53 -mcpu=cortex-a53 -fopt-info-vec-optimized -funsafe-math-optimizations -ffast-math

# Unoptimized Flags
NOOPT=-O0 -march=armv8-a+nosimd -mtune=cortex-a53 -mcpu=cortex-a53


------------------------------
dir: ./ML2
L2 resident (depending on LFSR settings) linked list traversal
Loop Seconds: 0.001831883
------------------------------
dir: ./EF
Floating-point Execution -- 8 Independent instructions per Iteration
Loop Seconds: 0.010340563
------------------------------
dir: ./ML2_BW_ldst
L2 Resident linked-list traversal -- B/W Limited (load/stores)
Loop Seconds: 0.005581387
------------------------------
dir: ./MM_st
Non-cache resident linked-list traversal (with stores)
size of long 8B
size of cache line: 64B
Allocated: 2097216B or 2MB
Loop Seconds: 0.008324731
------------------------------
dir: ./STL2
Repeatedly store, L2 Cache Resident
Loop Seconds: 0.011543803
------------------------------
dir: ./EI
Integer Execution -- 8 Independent computations per iteration 
Loop Seconds: 0.005831683
------------------------------
dir: ./DPTd
Simple Data Parallel Loop -- double sin() computation
Loop Seconds: 0.006046715
------------------------------
dir: ./MIM2
8 Streams independent Memory Access -- 2 coalescing ops per iteration
Loop Seconds: 0.005405220
------------------------------
dir: ./ML2_BW_st
L2 resident linked list traversal -- B/W limited (stores)
Loop Seconds: 0.001667096
------------------------------
dir: ./MD
Cache resident Linked List Traversal
Loop Seconds: 0.007284348
------------------------------
dir: ./CS1
Switch Case Statement of size 10 -- Different Case each time
Loop Seconds: 0.009244513
------------------------------
dir: ./MCS
Confict Misses with Stores (tune to your cache arch)
Loop Seconds: 0.007724431
------------------------------
dir: ./CCl
Impossible control with large basic blocks (potentially larger penalty)
Loop Seconds: 0.010115281
------------------------------
dir: ./ED1
Integer Execution -- Length 1 Dependency Chain per Iteration
Loop Seconds: 0.004791289
------------------------------
dir: ./MC
Conflict Misses (tune to your cache arch. by adding more streams)
Loop Seconds: 0.009226632
------------------------------
dir: ./STL2b
Repeatedly Store, L2 Resident (occasional stores)
Loop Seconds: 0.007380668
------------------------------
dir: ./DP1d
Simple Data Parallel Loop - Double Arithmetic
Loop Seconds: 0.010582208
------------------------------
dir: ./ML2_st
L2 resident linked list traversal (stores)
Loop Seconds: 0.006802200
------------------------------
dir: ./CRf
Recursive control flow - Fibonacci
Loop Seconds: 0.007431932
------------------------------
dir: ./DP1f
Simple Data Parallel Loop -- Float Arithmetic
Loop Seconds: 0.013674377
------------------------------
dir: ./CRd
Recursive Control Flow -- 1000 Deep
Loop Seconds: 0.007012250
------------------------------
dir: ./MIP
Large Instruction Region -- Instruction cache Misses
Loop Seconds: 0.006957465
------------------------------
dir: ./EM1
Integer Execution -- Length 1 Dependency Chain each loop (with multiplies)
Loop Seconds: 0.005691579
------------------------------
dir: ./M_Dyn
Load Store Access with Differeing dynmaic dependences.
Loop Seconds: 0.004785879
------------------------------
dir: ./CS3
Switch Case Statement of Size 10 -- Different case every third time
Loop Seconds: 0.009187708
------------------------------
dir: ./DPT
Simple Data Parallel Loop -- sin() computation
Loop Seconds: 0.006509781
------------------------------
dir: ./CCh
Random Control Flow -- Impossible to predict
Loop Seconds: 0.006080548
------------------------------
dir: ./DPcvt
Simple Data Parallel Loop -- Simple Data PArallel Loop Float/Double Conversion
Loop Seconds: 0.013172026
------------------------------
dir: ./results
