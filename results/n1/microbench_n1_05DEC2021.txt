================ microbench ==============
Date: Sun Dec 5 12:33:17 PST 2021
Kernel: Linux comhpcalt 5.4.0-88-generic #99-Ubuntu SMP Thu Sep 23 17:30:35 UTC 2021 aarch64 aarch64 aarch64 GNU/Linux
   Static hostname: comhpcalt
         Icon name: computer-server
           Chassis: server
        Machine ID: 62e5d310f0ae4c01bdfcc6f83853ed31
           Boot ID: 8896e5cb34594c40b115ef76dbd460e0
  Operating System: Ubuntu 20.04.3 LTS
            Kernel: Linux 5.4.0-88-generic
      Architecture: arm64

Architecture:                    aarch64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
CPU(s):                          32
On-line CPU(s) list:             0-31
Thread(s) per core:              1
Core(s) per socket:              32
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       ARM
Model:                           1
Model name:                      Neoverse-N1
Stepping:                        r3p1
CPU max MHz:                     1500.0000
CPU min MHz:                     1000.0000
BogoMIPS:                        50.00
L1d cache:                       2 MiB
L1i cache:                       2 MiB
L2 cache:                        32 MiB
NUMA node0 CPU(s):               0-31
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Not affected
Vulnerability Mds:               Not affected
Vulnerability Meltdown:          Not affected
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled via prctl
Vulnerability Spectre v1:        Mitigation; __user pointer sanitization
Vulnerability Spectre v2:        Not affected
Vulnerability Srbds:             Not affected
Vulnerability Tsx async abort:   Not affected
Flags:                           fp asimd evtstrm aes pmull sha1 sha2 crc32 atomics fphp asimdhp cpuid asimdrdm lrcpc dcpop asimddp ssbs

-- Compiler options:

# Compiler
CC=aarch64-linux-gnu-gcc

# Optimized Flags
# OPT=-O3 -march=armv8-a+simd -mtune=cortex-a53 -mcpu=cortex-a53 -ftree-vectorize -fopt-info-vec-optimized -funsafe-math-optimizations -ffast-math
OPT=-Ofast -mcpu=native -march=armv8.2-a+simd+nosve -fopt-info-all-vec 

# Unoptimized Flags
# NOOPT=-O0 -march=armv8-a -mtune=cortex-a53 -mcpu=cortex-a53
NOOPT=-O0 -march=armv8.2-a -mtune=neoverse-n1



------------------------------
dir: ./CS3
Switch Case Statement of Size 10 -- Different case every third time
NOOPT
Loop Seconds: 0.004337560
OPT
Loop Seconds: 0.003499600
------------------------------
dir: ./MC
Conflict Misses (tune to your cache arch. by adding more streams)
NOOPT
Loop Seconds: 0.003111000
OPT
Loop Seconds: 0.000272760
------------------------------
dir: ./CRf
Recursive control flow - Fibonacci
NOOPT
Loop Seconds: 0.001722440
OPT
Loop Seconds: 0.001045040
------------------------------
dir: ./CF1
Inlining Test for functions containing loops.
NOOPT
Loop Seconds: 0.000971080
OPT
Loop Seconds: 0.000273400
------------------------------
dir: ./ML2_BW_ld
L2 Resident linked list traversal -- B/W limited (loads)
NOOPT
Loop Seconds: 0.000593600
OPT
Loop Seconds: 0.000327960
------------------------------
dir: ./STL2b
Repeatedly Store, L2 Resident (occasional stores)
NOOPT
Loop Seconds: 0.004713640
OPT
Loop Seconds: 0.001717600
------------------------------
dir: ./CCl
Impossible control with large basic blocks (potentially larger penalty)
NOOPT
Loop Seconds: 0.006814600
OPT
Loop Seconds: 0.001118760
------------------------------
dir: ./CRd
Recursive Control Flow -- 1000 Deep
NOOPT
Loop Seconds: 0.001589080
OPT
Loop Seconds: 0.000939600
------------------------------
dir: ./STL2
Repeatedly store, L2 Cache Resident
NOOPT
Loop Seconds: 0.003418200
OPT
Loop Seconds: 0.001624080
------------------------------
dir: ./ML2_st
L2 resident linked list traversal (stores)
NOOPT
Loop Seconds: 0.002931160
OPT
Loop Seconds: 0.002341840
------------------------------
dir: ./ML2
L2 resident (depending on LFSR settings) linked list traversal
NOOPT
Loop Seconds: 0.002170360
OPT
Loop Seconds: 0.001532120
------------------------------
dir: ./EM5
Integer Execution -- Length 5 Dependency Chain each loop (with multiplies)
NOOPT
Loop Seconds: 0.001573080
OPT
Loop Seconds: 0.000874000
------------------------------
dir: ./CCh_st
Impossible to predict control + interaction with stores
NOOPT
Loop Seconds: 0.004477160
OPT
Loop Seconds: 0.000996480
------------------------------
dir: ./DPTd
Simple Data Parallel Loop -- double sin() computation
NOOPT
Loop Seconds: 0.001201120
OPT
Loop Seconds: 0.000979400
------------------------------
dir: ./CCh
Random Control Flow -- Impossible to predict
NOOPT
Loop Seconds: 0.003216760
OPT
Loop Seconds: 0.000746320
------------------------------
dir: ./MIM
8 Streams independent memory access, no conflicts
NOOPT
Loop Seconds: 0.003856560
OPT
Loop Seconds: 0.007241680
------------------------------
dir: ./MD
Cache resident Linked List Traversal
NOOPT
Loop Seconds: 0.004365880
OPT
Loop Seconds: 0.001416680
------------------------------
dir: ./STc
Repeatedly Store in consecutive access - L1 Cache
NOOPT
Loop Seconds: 0.007276080
OPT
Loop Seconds: 0.000411600
------------------------------
dir: ./CCm
Heavily Biased Branches
NOOPT
Loop Seconds: 0.002421560
OPT
Loop Seconds: 0.000785880
------------------------------
dir: ./DP1f
Simple Data Parallel Loop -- Float Arithmetic
NOOPT
Loop Seconds: 0.003962320
OPT
Loop Seconds: 0.000257720
------------------------------
dir: ./CRm
Merge sort.
NOOPT
Loop Seconds: 0.000922120
OPT
Loop Seconds: 0.000439440
------------------------------
dir: ./MM
Non-cache resident linked-list traversal
NOOPT
size of long 8B
size of cache line: 64B
Allocated: 2097216B or 2MB
Loop Seconds: 0.002665520
OPT
size of long 8B
size of cache line: 64B
Allocated: 2097216B or 2MB
Loop Seconds: 0.001295440
------------------------------
dir: ./MCS
Confict Misses with Stores (tune to your cache arch)
NOOPT
Loop Seconds: 0.001723400
OPT
Loop Seconds: 0.000765960
------------------------------
dir: ./M_Dyn
Load Store Access with Differeing dynmaic dependences.
NOOPT
Loop Seconds: 0.000888400
OPT
Loop Seconds: 0.000470120
------------------------------
dir: ./DPT
Simple Data Parallel Loop -- sin() computation
NOOPT
Loop Seconds: 0.001240720
OPT
Loop Seconds: 0.000616920
------------------------------
dir: ./MI
8 Streams of independent memory access, all cache resident.
NOOPT
Loop Seconds: 0.004266000
OPT
Loop Seconds: 0.000415280
------------------------------
dir: ./MM_st
Non-cache resident linked-list traversal (with stores)
NOOPT
size of long 8B
size of cache line: 64B
Allocated: 2097216B or 2MB
Loop Seconds: 0.001374440
OPT
size of long 8B
size of cache line: 64B
Allocated: 2097216B or 2MB
Loop Seconds: 0.000813280
------------------------------
dir: ./DPcvt
Simple Data Parallel Loop -- Simple Data PArallel Loop Float/Double Conversion
NOOPT
Loop Seconds: 0.003987000
OPT
Loop Seconds: 0.000395920
------------------------------
dir: ./EM1
Integer Execution -- Length 1 Dependency Chain each loop (with multiplies)
NOOPT
Loop Seconds: 0.002228920
OPT
Loop Seconds: 0.000699200
------------------------------
dir: ./CCa
Completely Biased Branch
NOOPT
Loop Seconds: 0.003241640
OPT
Loop Seconds: 0.001057640
------------------------------
dir: ./results
